"So be it! Let my life fuel the spell that ends his!"

 redwings Update Log  


The latest changes are at the top, and the less latest changes are at the bottom.

31 December 2021
- Renewable Familiars You can now renew your Summoning familiar by Summon-ing an extra pouch of the same Familiar, refreshing their duration to its maximum. Extend Beasts of Burden for longer trips, or your Golem to keep the Mining Boost up!

- Beasts of Burden will now drop their currently held items on dismissal, even if the player is an Iron. 

- Battlestaff price correction to 7k ea 

- Slimy eel fishing spots have been added

- the cope boxes have been disabled we will see him next time

- Clue Scroll Overhaul All monsters that can drop Clue Scrolls have now been corrected to drop the right "clue" drop. No longer will everything always force an Easy Clue. 

27 December 2021

- Secondary banks are now accessible at a hefty fee of 5 million GP, doubling your bank space from 496 to 992. You can switch between bank accounts at a bank teller.

- A new chinning/bursting spot has been made accessible. The Skeleton Monkeys inside Ape Atoll Dungeon can now be fought, and the area is now a multicombat area

- Giant Mole's drop table has been cleaned up, with the Thanos Drop Editor back online.

- The Oo'glog Fresh Meat store is now accessible, and has an increased stock of 100 raw bird meat.

- The damage modifier (A thing mostly used in special attacks to modify damage) is now correctly factored in, for all 3 combat styles.   

- You can now obtain Mort Myre Pears and stems from casting Bloom, and Bloom's sfx has been added.

- The Fishing Guild can now be entered via skill boosts. 

- Edgeville Canoe -> WIlderness Pond travel option now fixed

26 December 2021

- Ruby bolts(e) fix!(kinda)!! Ruby bolt(e) special attack, Blood Forfeit, maximum current Hitpoint damage cap has been increased from 35 to a kiiinda close enough cap of 150 Hitpoints. (The reason why the cap can't be removed yet, is that the Corporeal Beast-specific damage cap is not implemented) 

- The Cider, needed to complete a step in Seers Village Diary, can now be purchased from our bandaid-fix Digsite Exam Center shop. Make sure to take 5, and try not to left-click Drink them

25 December 2021
- Seer's Achievement Diaries should now be fully completable.
- Enhanced Excalibur now has all of it's special attack effects implemented 
- Blast Furnace should now reward 1 smithing XP to everyone in the Blast Furnace area for each bar that is smithed.
- The AFK timer has been removed, may Evil Chicken have mercy on your soul.

- Mobile Client Update
- The Android Client has been updated.
- App strings updated to match the rebrand, new app icon replacement still pending.
- To use redwings-mobile to connect to other servers (such as local dev instances or the main 2009scape game), download and edit the Android Client Config, load the Android Client, tap the rightmost corner in-app to bring up the Settings menu, and tap LOAD CONFIG. 

24 December 2021
- THE FUNNY FURNACE UPDATE(and friends)
- What's new?
- Blast Furnace is now complete enough to be considered complete™️
- Ordan and Jorzik's dialogue has been implemented.
- Jorzik's armour store is in as well so now you have a place to dump all of your blast furnace creations
- Some of the funny furnace funny logic has been polished up and the kinks worked out
- The steps to the blast furnace in Keldagrim will now gatekeep players with under 60 smithing as intended
- If you're under 60 smithing you can pay 2500gp for ten minutes of access to the blast furnace
- If you have the Ring of Charos(a) equipped the fee is lowered to 1250gp
- Green Dragon bots can hop over the wildy ditch again but bots in general are still fucked
- The correct item spawns have been added to the Blast Furnace area
- The Blast Furnace will now remember what you have stored into it
- You now have to pedal your ores into the ore pot before you can smelt them

- Explorer's Ring now has correct sfx for recharging your run energy, as well as the Falador Farm Cabbage-port. Leaf-bladed sword now has correct sfx.

- Family Crest Update! Fractals polished the last bits of the quest up. The bossfight, Chronozon, now works as it should, and the perfect gold ore puzzle has been added.

- Client Update!
1. december_snow is now accessible, allowing you to toggle on/off the game-wide snow effect. 
2. left_click_attack This allows you, no matter what, to have any monster's main left-click option be Attack. This is important for bossing, as bosses usually have insanely high Combat levels, thus usually only being right-clickable.  

23 December 2021 
- The Xmas Gift Limit has been increased! To compensate for missing most of December, the daily gift limit has been increased to 28.

- Permadeath Ironmen should now be able to access the 20x rates from Hans in Tutorial Cellar/Lumbridge Castle. Take on the challenge, and rebuild faster than ever.  

- sniffbot is back The Adventure Bot dialogue has been restored

- At the Digsite Exam Centre, south of the Digsite, there is a Researcher you can talk to to access a shop "Forgotten Relix" which contains until now inacessible quest-unlocked items such as the Magic Secateurs, Salve Amulet, and Iban's Staff.  

- Bill Teach, Once Lost Uncle of the Fellers, has returned to take us on adventures near and far. At the Port Phasmatys pub, you can talk to him and he will take you where you want to go.  

- Barrows gloves return from the crypt! The Culinaromancer's Chest, the RFD gloves, and the bankchest in Lumbridge Basement are now accessible to all accounts.

- Blast Furnace Exclusive Beta The Blast Furnace is now accessible deep within Keldagrim. Smithing skill trainers are weeping, overjoyed. 