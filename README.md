[![AGPL-3.0 License][license-shield]][license-url]

<br />
<p align="center">
  <a href="https://gitlab.com/skelsoft/redwings">
    <img src="https://i.imgur.com/oBL6JXv.png" alt="Logo" width="500" height="150">
  </a>
  <h3 align="center">An open source MMORPG emulation server</h3>
  <p align="center">
    <br />
    <br />
    <a href="https://2009scape.org/">The Official 2009scape Server</a>
    ·
    <a href="https://discord.gg/43YPGND">2009Scape Discord Invite</a>
    ·    <a href="https://gitlab.com/skelsoft/redwings/-/issues">Report Bug</a>
  </p>
</p>

## Table of Contents

* [redwings Information](#redwings-information)
* [Contributing](#contributing-to-the-redwings-codebase)
* [License](#license)


## redwings Information

*"Captain! We are the Red Wings, the pride of Baron! Must we be thieves sent to plunder the weak?"*


Welcome to redwings, powered by 2009scape. For up-to-date changes to the redwings codebase, please refer to our [CHANGELOG.md](CHANGELOG.md).

Redwings was born from the 2009scape project in late December 2021. Restoring removed features, collaborating and sourcing data, and compensating for the lack of certain content. Where we go now isn't up to me, but whoever decides to partake in developing, free from constraints.

The redwings logos and icons are © property of the redwings development team.


## Contributing to the redwings codebase
To refer to setup guides and insructions in regards to working on, and contributing to the redwings codebase, please refer to our [CONTRIBUTE.md](CONTRIBUTE.md).


### License

The redwings codebase is licensed under the AGPL 3.0 license, which can be found <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">here.</a>


[license-shield]: https://img.shields.io/badge/license-AGPL--3.0-informational
[license-url]: https://www.gnu.org/licenses/agpl-3.0.en.html
